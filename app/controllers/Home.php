dsw<?php 

class Home extends Controller{
    public function index()
    {   
        $data['title'] = 'Home';
        $data['nama'] = $this->model('HomeModel')->getUser();
        $data['users'] = $this->model('UserModel')->getAllUser();

        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer', $data);

    }

    public function about($company = 'SMKN 1')
    {
        $data['title'] = 'Home';
        $data['name'] = $this->model('HomeModel')->getAlluser();


        $data["judul"]="About";
        $data["company"]= $company;
        $this->view('template/header', $data);
        $this->view('home/index',$data);
        $this->view('template/footer',$data);
    }
}